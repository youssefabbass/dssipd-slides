---
marp: true
---

# DSSIPD-SoSe22 - Class 01
## Introduction to RWTH IT & Laptop Setup

---
# Agenda

- Logins
  - Classroom account
  - Online accounts
- Websites
  - RWTH websites
  - Third-party websites
- Software
  - Installers
  - Programming environments
  - Scripts and tweaks
  - Recommendations

---

## Online & non-online accounts


> Accounts, credentials or logins are referred to using the at `@` symbol. This is to help you not mix-up different logins when a large number of IT platforms are introduced at the same time.

- RWTH SSO (Single-Sign On) **@rwth-sso**
- RWTH VPN account          **@rwth-vpn**
- Discord account           **@discord**
- GitLab account            **@gitlab**
- GitHub account            **@github**
- Docker Hub account        **@dockerhub**
- Classroom windows account **@classroom**
  - Initial, temporary assword: `CR_student`

---
## Online platforms (i.e. links)

> Places are referred to using the hashtag `#`symbol. This helps us be more clear when referring to teaching materials and resources.

- [RWTH Self-Service](https://www.rwth-aachen.de/selfservice)   **#selfservice**
- [Moodle](https://moodle.rwth-aachen.de)                       **#moodle**
  - [DSSIPD class on Moodle](https://moodle.rwth-aachen.de/course/view.php?id=23977)    **#moodle-ddsipd**
  - [DDP class on Moodle](https://moodle.rwth-aachen.de/course/view.php?id=23976)       **#moodle-ddp**
- [Discord channel](https://discord.gg/UkjvmXf4)                **#discord-channel**
- [GitLab group](https://gitlab.com/rwth-crmasters-sose22)      **#gitlab-group**
- [Docker Hub](https://hub.docker.com/)                         **#dockerhub**

---

## Essential software

> This is the software we absolutely need for the DDP class and its supporting module. This software can be installed on Windows, MacOSX or Linux. Unfortunately, we are not able to provide detailed installation instructions for all systems.

- Discord
- Docker Desktop
- git
- Visual Studio Code
- Terminal application (e.g. Windows Terminal, cmder, etc.) 
- nvm
- Anaconda

---

## Additional Software

- Fancy prompts for the terminal (Oh My Posh, starship, etc.)
- Bitwarden firefox/chrome browser plugin
- "I don't care about cookies" firefox/chrome browser plugin

---

## Class experience

- Login to laptop and change password (using your @classroom login)
- Login to **#selfservice** and change [VPN password](https://moodle.rwth-aachen.de/mod/page/view.php?id=922426#yui_3_17_2_1_1649242681241_572), these are the @rwth-vpn logins
- Login to **#moodle** with @rwth-sso logins
- Browse to **#moodle-ddp** and **#moodle-ddsipd** (also called, the _Moodle rooms_)
- Introduce the **#discord-channel**
- Introduce the **#gitlab-group**
- Suggest that they star my Docker Hub repo `orwa84/githug` using their new @dockerhub logins

---

## Checklist for next class

- Check the new [DSSIPD study guide](https://moodle.rwth-aachen.de/mod/resource/view.php?id=933511) on Moodle
- Check the [assignment](https://moodle.rwth-aachen.de/mod/assign/view.php?id=933670) on Moodle
- The assignment has to be submitted before _12.04.2022 at midnight_.
- If you get stuck, please reach out to others on the #i-am-stuck room, on the Discord server.