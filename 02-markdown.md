---
marp: true
---

# DSSIPD-SoSe22 - Class 02
## Markdown

---

# Before we Begin

- Amazing turnaround! Only 2 people still did not submit the last assignment.
- Thank you for being amazing students!

---

# Agenda

- Self-study
- Markdown basic features
- Markdown on GitLab
- Markdown in vscode
- Markdown in websites
- Markdown in slides

---

## Self-study


In DSSIPD, the teacher is merely a facilitator of your self-learning.

Self-learning is best achieved via rapid cycles of reading and applying the knowledge taught using the right channels.

In today's class, we demonstrate the way to self learn using the following experimentation channels:
- Online Markdown and extension parsers (e.g. Kramdown, PlantUML, Mermaid)
- Gitlab Markdown Editor (i.e. Web IDE)
- Vscode Markdown Preview (or Preview Enhanced)

---

## Some self-learning resources

- Videos (check the `Upload` folder on `cr-storage` in the classroom's network)
- [Online Markdown tutorial](https://www.markdowntutorial.com/) 
- [Kramdown](https://kramdown.gettalong.org/quickref.html)
- [GitLab-flavour Markdown](https://docs.gitlab.com/ee/user/markdown.html)
- [PlantUML diagrams](https://plantuml.com/sitemap-language-specification), which can be used inside of a code block on GitLab
- [Mermaid diagrams](https://mermaid.live), which can be used inside of a code block on GitLab.

---

## Markdown using GitLab

Please visit the [course-work](https://gitlab.com/rwth-crmasters-sose22/course-work) subgroup on GitLab.

From there, navigate to the [gitlab-markdown](https://gitlab.com/rwth-crmasters-sose22/course-work/gitlab-markdown) repository.

Follow the instructions shown in the `README.md` file.

---

## Class experience

- Go over the main basic Markdown features using the tutorial.
- Implement the experimentation using a personal file to be added to the [gitlab-markdown](https://gitlab.com/rwth-crmasters-sose22/course-work/gitlab-markdown) repository.
- Implement more features from GitLab-flavoured implementation, guided by the online documentation.
- Experiment with _PlantUML_ diagrams.
- Experiment with _Mermaid_ diagrams.